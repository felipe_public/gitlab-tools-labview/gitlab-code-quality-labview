# Changelog
All notable changes to this project will be documented in this file.

See [standard-version](https://github.com/conventional-changelog/standard-version) for commit
guidelines. This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).
## 1.0.0 (2024-05-30)


### ⚠ BREAKING CHANGES

* update all VIs with hooks, included check_name in report (#1)

### Features

* Unit Tests for most of the features. ([a56fe39](https://gitlab.com/felipe_public/gitlab-tools-labview/gitlab-code-quality-labview/commit/a56fe39f6002d928c063dfa4744745a0e754c592))
* update all VIs with hooks, included check_name in report ([#1](https://gitlab.com/felipe_public/gitlab-tools-labview/gitlab-code-quality-labview/issues/1)) ([4db1660](https://gitlab.com/felipe_public/gitlab-tools-labview/gitlab-code-quality-labview/commit/4db16602f6eb8578c089a83b646e7fd975eef0f0))
* update documentation and .vipb ([#1](https://gitlab.com/felipe_public/gitlab-tools-labview/gitlab-code-quality-labview/issues/1)) ([7310cae](https://gitlab.com/felipe_public/gitlab-tools-labview/gitlab-code-quality-labview/commit/7310caea59e0bf007c7443d685988d611b6ab64b))


### Bug Fixes

* updates example file ([3bea016](https://gitlab.com/felipe_public/gitlab-tools-labview/gitlab-code-quality-labview/commit/3bea0162c8877b8d0f1857c3b05b12a58b2055a3))


### Docs

* fixed picture paths ([#1](https://gitlab.com/felipe_public/gitlab-tools-labview/gitlab-code-quality-labview/issues/1)) ([16d1b09](https://gitlab.com/felipe_public/gitlab-tools-labview/gitlab-code-quality-labview/commit/16d1b09ea5b018e93b55d35c6a045f56e833cc89))


### Code Refactoring

* to test code quality report ([7ba73bb](https://gitlab.com/felipe_public/gitlab-tools-labview/gitlab-code-quality-labview/commit/7ba73bb118043708137bdf85d0cd0417179ec861))


### CI

* update pipeline ([577fd47](https://gitlab.com/felipe_public/gitlab-tools-labview/gitlab-code-quality-labview/commit/577fd47991ceefdfd172158347a5a0f098831135))

## [0.2.0] - 2020-12-04
### Added
- feat: Unit Tests for most of the features.
- feat: Fingerprint for each of the test failures.
- feat: Included Code Severity according to Gitlab 13.6 update.
- feat: Gitlab CI and CI-CD Tools as SDM Dependency.

### Changed
- fix: The Library name. Now the library is dedicated only to Code Quality.

## [0.1.0] - 09-20-2020
### Added
- Initial Version with Code Quality Report.
